import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the task number from 1 to 5:");
        int number = sc.nextInt();

        if (number == 1) {
            BaseConverter.convert();
        }

        if (number == 2) {
            ClassMap.convertMap();
        }

        if (number == 3) {
            Iter.Iter();
        }
        if (number == 4) {
            List collection = new ArrayList();
            collection.add("element 1");
            collection.add("element 2");
            collection.add("element 3");
            collection.add("element 3");


            NotDouble.NotDuplicates(collection);
        }

        if (number == 5) {
            Integer[] arr = new Integer[100];
            Fill.fill(arr,integer -> integer * integer);
        }
    }
}

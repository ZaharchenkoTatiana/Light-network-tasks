// Program to get an iterator over a primitive array in Java

import java.util.Iterator;
public abstract class Iter implements IIterator {
    static void Iter() {
        {

            int[] array = {4, 5, 6, 2};

            java.util.Iterator<Integer> iterator = new Iterator<Integer>()
            {
                private int i = 0;

                @Override
                public boolean hasNext() {
                    return array.length > i;
                }

                @Override
                public void remove() {

                }

                @Override
                public Integer next() {
                    return Integer.valueOf(array[i++]);
                }
            };

            iterator.forEachRemaining(System.out::println);
            }
        }
    }


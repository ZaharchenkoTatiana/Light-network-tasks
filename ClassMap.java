import java.util.*;
import java.util.stream.Collectors;

public class ClassMap{
    static Map<String, Integer> convertMap() {

        Scanner sc = new Scanner(System.in);
        Map<Integer, String> oldMap = new HashMap<Integer, String>();

        System.out.println("Enter the number of items: ");
        int k = sc.nextInt();

        String arr[] = new String[k];

        System.out.println("Enter the number of items " + k + " times: ");

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.next();
            oldMap.put(i, arr[i]);
        }

        System.out.println("oldMap: ");
        oldMap.entrySet().forEach(System.out::println);

        HashMap<String,Integer> swapped = new HashMap<String, Integer>();
        for(Map.Entry<Integer,String> entry : oldMap.entrySet())
            swapped.put(entry.getValue(), entry.getKey());

        //The values are output out of order, then:
        Map<String, Integer> sortedMap = swapped.entrySet().stream()
                .sorted(Comparator.comparingInt(e -> e.getValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> { throw new AssertionError(); },
                        LinkedHashMap::new
                ));

        System.out.println("New HashMap: ");
        sortedMap.entrySet().forEach(System.out::println);

        return swapped;
    }
}

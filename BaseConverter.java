import java.util.Arrays;
import java.util.Scanner;

public class BaseConverter {
        static void convert(){
            Scanner Scan = new Scanner(System.in);

            System.out.println("Input a celsius: ");
            double celsius = Scan.nextDouble();

            double fahrenheit = (celsius * 1.8) + 32;
            double kelvin = celsius + 273.15;

            System.out.println("Fahrenheit:" + fahrenheit);
            System.out.println("Kelvin:" + kelvin);

        }
}

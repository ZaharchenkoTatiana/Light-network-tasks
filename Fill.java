import java.util.function.Function;

public class Fill {
    public static <T> void fill(T[] objects, Function<Integer, ? extends T> function)
    {
        for(int i = 0; i < objects.length; i++)
        {
            objects[i] = function.apply(i);
        }
    }
}

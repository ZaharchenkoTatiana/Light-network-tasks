import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class NotDouble {

        public static <T > Collection < T > NotDuplicates(Collection < T > collection) {
        return new HashSet<>(collection);
    }
}

public interface IIterator<E>{
    E next();
    boolean hasNext();
    void remove();
}